local Tile = require "libs/tile"
local Object = require "libs/object"
local Actor = require "libs/actor"

-- local tilelist = require "tilelist"
local objectlist = require "objectlist"

local tilesize = 32

----------------------------------------------------------------------
-- Tilemap Class & Constructor ---------------------------------------
----------------------------------------------------------------------

local Tilemap = {}

-- Tilemap constructor.
-- Mandadatory argument: name must be a name for the Tilemap, as a string.
-- Optional argument: tiles can be from a loaded tilemap.
function Tilemap.new(name, width, height, tilelist, objectlist, actorlist, save)
	------------------------------------------------------------------
	-- Assertions, to make sure you're using this correctly ----------
	------------------------------------------------------------------
	assert(type(name) == "string", "Argument must be a map name, as a string!")
	assert(type(width) == "number", "Argument must be a number representing width!")
	assert(type(height) == "number", "Argument must be a number representing height!")

	------------------------------------------------------------------
	-- Some safe, out of scope, per object variables -----------------
	------------------------------------------------------------------
	local name = name -- Name of the Tilemap
	local width = width -- Width of the Tilemap
	local height = height -- Height of the Tilemap

	-- Create an empty
	local function init_layer(default)
		local layer = {}
		for x = 1, width do
			layer[x] = {}
			for y = 1, height do
				layer[x][y] = default
			end
		end
		return layer
	end

	-- Layers are accessed like layer[x][y] = thing
	local tiles = init_layer(tilelist.ground) -- Tiles layer
	local objects = init_layer() -- Objects layer
	local actors = init_layer() -- Actors layer

	------------------------------------------------------------------
	-- Tilemap Methods -----------------------------------------------
	------------------------------------------------------------------
	local tilemap = {}
	setmetatable(tilemap, Tilemap)

	-- Returns the name of the Tilemap
	function tilemap.getName()
		return name
	end

	-- Returns the current size of the Tilemap
	-- Return order: width, height, total number of tiles (some may be nil)
	function tilemap.getSize()
		local total = width * height
		return width, height, total
	end

	function tilemap.getPixelSize()
		return (width + 2) * tilesize, (height + 2) * tilesize
	end

	-- Returns the tile at x, y if it exists.
	-- Mandatory arguments: x, y must be numbers representing position.
	function tilemap.getTile(x, y)
		assert(type(x) == "number", "x, y, and z values are required!")
		assert(type(y) == "number", "x, y, and z values are required!")
		return tiles[x][y]
	end


	-- Sets the tile at x, y.
	-- Mandatory arguments: x, y must be numbers representing position.
	function tilemap.setTile(x, y, tile)
		assert(type(x) == "number", "x, y, and z values are required!")
		assert(type(y) == "number", "x, y, and z values are required!")
		assert(getmetatable(tile) == Tile, "tile must be a valid Tile class object.")
		if x >= 1 and x <= width and y >= 1 and y <= height then
			tiles[x][y] = tile
		end
	end

	-- Returns the object at x, y if it exists.
	-- Mandatory arguments: x, y must be numbers representing position.
	function tilemap.getObject(x, y)
		assert(type(x) == "number", "x, y, and z values are required!")
		assert(type(y) == "number", "x, y, and z values are required!")
		return objects[x][y]
	end

	-- Sets the object at x, y.
	-- Mandatory arguments: x, y must be numbers representing position.
	function tilemap.setObject(x, y, object)
		assert(type(x) == "number", "x, y, and z values are required!")
		assert(type(y) == "number", "x, y, and z values are required!")
		assert(x <= width, "The value of x is greater than the tilemap width")
		assert(y <= height, "The value of y is greater than the tilemap height")
		assert(getmetatable(object) == Object, "object must be a valid Object class object.")
		objects[x][y] = object
	end

	-- Returns the actor at x, y if it exists.
	-- Mandatory arguments: x, y must be numbers representing position.
	function tilemap.getActor(x, y)
		assert(type(x) == "number", "x, y, and z values are required!")
		assert(type(y) == "number", "x, y, and z values are required!")
		return actors[x][y]
	end

	-- Sets the actor at x, y.
	-- Mandatory arguments: x, y must be numbers representing position.
	function tilemap.setActor(x, y, actor)
		assert(type(x) == "number", "x, y, and z values are required!")
		assert(type(y) == "number", "x, y, and z values are required!")
		assert(x <= width, "The value of x is greater than the tilemap width")
		assert(y <= height, "The value of y is greater than the tilemap height")
		assert(getmetatable(actor) == Actor, "actor must be a valid Actor class object.")
		actors[x][y] = actor
	end

	function tilemap.collide(x, y)
		if x <= 0 or y <= 0 or x > width or y > height then
			return true
		end
		if tiles[x][y].collide == true then
			return true
		elseif objects[x][y] and objects[x][y].collide == true then
			return true
		elseif actors[x][y] and actors[x][y].collide == true then
			return true
		end
		return false
	end

	function tilemap.interact(x, y)
		if x <= 0 or y <= 0 or x > width or y > height then
			return
		end

		if objects[x][y] and objects[x][y].interact then
			local list = objects[x][y]:interact()
			if list.remove == true then
				objects[x][y] = nil
			end
			if list.drop then
				objects[x][y] = objectlist[list.drop]
			end
			return list
		elseif tiles[x][y] and tiles[x][y].interact then
			tiles[x][y]:interact()
		end
	end

	-- An iterator to be used in for loops.
	-- Returns a control, x, y values.
	function tilemap.itiles()
		local control_end = width * height
		local control = -1

		local function iterator(tiles, control)
			control = control + 1
			if control == control_end then return nil end
			local y = control % height
			local x = (control - y)/height % width
			return control, x + 1, y + 1
		end

		return iterator, tiles, control
	end

	function tilemap.draw()
		-- Draw the tiles in the map
		local prev_color = {love.graphics.getColor()}

		for _, x, y in tilemap.itiles() do
			local tile = tiles[x][y]
			if tile.image then
				love.graphics.draw(
					tile.image,
					x*tilesize,
					y*tilesize
				)
			elseif tile.color then
				love.graphics.setColor(tile.color)
				love.graphics.rectangle(
					"fill",
					x*tilesize,
					y*tilesize,
					tilesize,
					tilesize
				)
				love.graphics.setColor(prev_color)
			end
		end

		for _, x, y in tilemap.itiles() do
			local object = objects[x][y]
			if object then
				love.graphics.draw(
					object.image,
					x*tilesize,
					y*tilesize
				)
			end
		end

		-- Draw the Actors in the tilemaps
		for ak, av in pairs(Actor.list) do
			love.graphics.draw(
				av.image, -- image
				av.drawPos.x*tilesize + tilesize/2, -- x
				av.drawPos.y*tilesize + tilesize/2, -- y
				av.dir, -- rotation
				1, -- scale
				1, -- scale
				tilesize/2, -- x offset
				tilesize/2 -- y offset
			)
		end
	end

	return tilemap
end

-- NEEDS REWRITE FOR USE!
-- Construct a new Tilemap from a file.
-- Mandatory argument: filename must be for an existing file.
-- This loads external code, and can fuck things up if you let it.
function Tilemap.load(filename)
	assert(type(filename) == "string", "Argument must be a filename, as a string")
	assert(love.filesystem.exists(filename), "The given file does not exist!")
	local loaded = love.filesystem.load(filename)()
	local object = Tilemap.new(loaded.name, loaded.width, loaded.height, loaded.tiles)
	return object
end

-- NEEDS REWRITE FOR USE!
-- Tilemap writer
function Tilemap.save(tilemap, filename)
	assert(getmetatable(tilemap) == Tilemap, "This is not a polytile Tilemap object!")
	if not filename then filename = "maps/"..map.name..".lua" end
	-- I dunno, write the map.
end

return Tilemap
