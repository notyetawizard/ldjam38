----------------------------------------------------------------------
-- Tile Class & Constructor ------------------------------------------
----------------------------------------------------------------------
local Tile = {} -- Class, exported

-- Tile constructor.
function Tile.new(options)
	------------------------------------------------------------------
	-- Assertions, to make sure you're using this correctly ----------
	------------------------------------------------------------------
	assert(type(options.name) == "string", "options.name must be a tileset name, as a string.")
	--assert(type(options.image) == "string", "options.image must be a valid filename.")
	--assert(love.filesystem.exists(options.image), "options.image must be a valid filename.")
	assert(type(options.collide) == "boolean", "options.collide must be a booleen.")

	------------------------------------------------------------------
	-- Tile Creation -------------------------------------------------
	------------------------------------------------------------------
	local tile = {}
	setmetatable(tile, Tile)
	for k, v in pairs(options) do
		tile[k] = v
	end

	return tile
end

return Tile
