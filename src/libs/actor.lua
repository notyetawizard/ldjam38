local Image = require "libs/image"

local Actor = {}
Actor.count = 0
Actor.list = {}

movespeed = 4

function Actor:new(tilemap)
    o = {}
    setmetatable(o, self)
    self.__index = self
    Actor.count = Actor.count + 1
    Actor.list[Actor.count] = o

    o.pos = {
        x = 0,
        y = 0
    }
    o.drawPos = {
        x = 0,
        y = 0
    }
    o.animating = false
    o.image = Image.get("actor")
    o.map = tilemap
    o.items = {}
    o.dir = 0

    return o
end

function Actor:move(dx, dy)
    if not self.animating then
        result = self.map.interact(self.pos.x + dx, self.pos.y + dy)
        if result and result.give then
            if self.items[result.give] == nil then
                self.items[result.give] = 0
            end
            self.items[result.give] = self.items[result.give] + result.amount
            print("have "..self.items[result.give].." "..result.give.."s")
        end

        if not self.map.collide(self.pos.x + dx, self.pos.y + dy) then
            self.pos.x = self.pos.x + dx
            self.pos.y = self.pos.y + dy
            self.animating = true
            --print("position: "..self.pos.x..", "..self.pos.y)
        end
    end
end

function Actor:setPos(x, y)
    self.pos.x = x
    self.pos.y = y
    self.drawPos.x = x
    self.drawPos.y = y
end

function Actor:update(dt)
    if self.animating then
        local goalPos = {
            x = self.pos.x,
            y = self.pos.y
        }
        local deltaPos = {
            x = goalPos.x - self.drawPos.x,
            y = goalPos.y - self.drawPos.y
        }
        local direction = {
            x = (deltaPos.x<0 and -1) or 1,
            y = (deltaPos.y<0 and -1) or 1
        }
        local move = {
            x = math.min(math.abs(deltaPos.x), movespeed * dt) * direction.x,
            y = math.min(math.abs(deltaPos.y), movespeed * dt) * direction.y
        }
        self.drawPos.x = self.drawPos.x + move.x
        self.drawPos.y = self.drawPos.y + move.y

        self.animating = not (self.pos.x == self.drawPos.x and
                              self.pos.y == self.drawPos.y)

        --print("drawPos: "..self.drawPos.x..", "..self.drawPos.y)
    end
end

return Actor
