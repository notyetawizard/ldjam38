local Image = require "libs/image"
local Actor = require "libs/actor"
local Tilemap = require "libs/tilemap"

local Player = {}

function Player.new(map)
    local player = Actor:new(map)
    player.image = Image.get("player")
    return player
end

return Player
