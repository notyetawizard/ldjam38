objectlist = require "objectlist"

Toolbar = {}
Toolbar.height = 32

local font = love.graphics.newFont(32)

function Toolbar:new(player)
    o = {}
    setmetatable(o, self)
    self.__index = self

    o.player = player

    return o
end

function Toolbar:draw()
    local x = 0
    local y = 0

    love.graphics.setFont(font)

    for k, v in pairs(self.player.items) do
        image = objectlist[k].image
        love.graphics.draw(image, x, y)
        x = x + image:getWidth()
        text = love.graphics.newText(font, ""..v)
        love.graphics.draw(text, x, y)
        x = x + text:getWidth()
    end
end

return Toolbar
