Image = {}
Image.cache = {}

function Image.get(name)
    local img = Image.cache[name]
    if img == nil then
        img = love.graphics.newImage("assets/"..name..".png")
        Image.cache[name] = img
    end
    return img
end

return Image
