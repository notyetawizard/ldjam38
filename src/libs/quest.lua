local Messages = require "libs/messages"

local tilelist = require "tilelist"
local objectlist = require "objectlist"

----------------------------------------------------------------------
-- Quest Class & Constructor -----------------------------------------
----------------------------------------------------------------------
local Quest = {
	scienceQuest = {
		[1] = {
			type = "science",
			name = "Get the food",
			start = function (self)
				self.curStep = 1
				self.startPos = {
					x = self.player.pos.x,
					y = self.player.pos.y
				}
			end,
			steps = {
				[1] = function (self)
					if math.abs(self.startPos.x - self.player.pos.x)
							+ math.abs(self.startPos.y - self.player.pos.y) > 5 then
						Messages.new("A giant hand decends from the sky!", function()
							Messages.new("It leaves behind an apple.")
						end)
						local width, height = self.tilemap.getSize()
						self.tilemap.setObject(self.startPos.x, self.startPos.y, objectlist["apple"])
						return true
					end
				end,
				[2] = function (self)
					self.update = function (self)
						if self.player.items["apple"] == 1 then
							return true
						end
					end
			    end,
			},
			update = function (self)
				if self.steps[self.curStep](self) then
					self.curStep = self.curStep + 1
					if self.steps[self.curStep] == nil then
						return true
					end
				end
			end,
			finish = function (self)
				Messages.new("Yum!")
			end
		}
	},
	active = {},
	completed = {},
	freetime = 0
} -- Class, exported

Quest.escapeQuest = {
	[1] = {
		type = "main",
		name = "Escape the Cage",
		runtime = 0,
		start = function (self)
			local width, height, total = self.tilemap.getSize()

			self.player:setPos(width/2, height/2)

			for i = 1, math.random(3, 6) do
				while true do
					local wx, wy = math.random(1, width), math.random(1, height)
					if (wx <= width/2 - width/8 or wx >= width/2 + width/8)
					and (wy <= height/2 - height/8 or wy >= height/2 + height/8) then
						local wdx, wdy = math.random(1, width/8), math.random(1, height/8)
						for _, x, y in self.tilemap.itiles() do
							if x >= (wx - wdx) and x <= (wx + wdx)
							and y >= (wy - wdy) and y <= (wy + wdy)
							and (x + y)/2 > (wx - wdx + wy - wdy)/2
							and (x + y)/2 < (wx + wdx + wy + wdy)/2 then
								self.tilemap.setTile(x, y, tilelist["water"])
							end
						end
						break
					end
				end
			end

			while true do
				local wx, wy = math.random(1, width), math.random(1, height)
				if self.tilemap.getTile(wx, wy).name == "ground"
				and (wx <= width/2 - width/8 or wx >= width/2 + width/8)
				and (wy <= height/2 - height/8 or wy >= height/2 + height/8) then
					for x = wx - 2, wx + 2 do
						self.tilemap.setTile(x, wy - 2, tilelist["wall"])
						self.tilemap.setTile(x, wy + 2, tilelist["wall"])
					end
					for y = wy - 1, wy + 1 do
						self.tilemap.setTile(wx - 2, y, tilelist["wall"])
						self.tilemap.setTile(wx + 2, y, tilelist["wall"])
					end
					break
				end
			end

			local nrocks = 0
			while nrocks < 5 do
				for _, x, y in self.tilemap.itiles() do
					if self.tilemap.getTile(x, y).name == "ground" then
						if nrocks < 15 and math.random(1, 50) == 1 then
							nrocks = nrocks + 1
							self.tilemap.setObject(x, y, objectlist["rock"])
						end
					end
				end
			end

			Messages.new("Build a Hot Air Balloon\nand escape from the scientists!\n\n"
			.. "You'll need a Basket, Balloon,\nand a Match")
		end,
		update = function (self, dt)
			self.runtime = self.runtime + dt
			if Quest.freetime > 0 then
				if not self.player.items["hot_air_balloon_basket"]
				and self.runtime > 120 then
					Quest:start(Quest.escapeQuest[2])
				elseif not self.player.items["hot_air_balloon_balloon"]
				and self.runtime > 240 then
					Quest:start(Quest.escapeQuest[3])
				elseif not self.player.items["hot_air_balloon_match"]
				and self.runtime > 360 then
					Quest:start(Quest.escapeQuest[4])
				end
			end
			if self.player.items["hot_air_balloon"] then
				return true
			end
		end,
		finish = function (self)
			Messages.new("Congratulations! You escaped!")
		end
	},
	[2] = {
		type = "main",
		name = "Collect a Basket",
		start = function (self)
			Messages.new("Hey; is that Basket? Grab that!")
		end,
		update = function (self, dt)
			if self.player.items["hot_air_balloon_basket"] then
				return true
			end
		end,
		finish = function (self)
			Messages.new("You got the Basket!")
		end
	},
	[3] = {
		type = "main",
		name = "Collect a Balloon",
		start = function (self)
			Messages.new("Hey; is that Balloon? Grab that!")
		end,
		update = function (self, dt)
			if self.player.items["hot_air_balloon_balloon"] then
				return true
			end
		end,
		finish = function (self, dt)
			Messages.new("You got the Balloon!")
		end
	},
	[4] = {
		type = "main",
		name = "Collect a Match",
		start = function (self)
			Messages.new("Hey; is that Match? Grab that!")
		end,
		update = function (self)
			if self.player.items["hot_air_balloon_match"] then
				return true
			end
		end,
		finish = function (self)
			Messages.new("You got the Match!")
		end
	}
}


function Quest:setup(tilemap, player)
	self.tilemap = tilemap
	self.player = player

	self.curScienceQuest = 0
	self.curEscpaeQuest = 1

	self.nextQuestTime = 10

	self:start(self.escapeQuest[self.curEscpaeQuest])
end

function Quest:start(quest)
	quest.tilemap = self.tilemap
	quest.player = self.player

	table.insert(self.active, quest)
	quest:start()
end

function Quest:finish(quest)
	quest:finish()
	for k, v in ipairs(self.active) do
		if v.name == quest.name then
			table.remove(self.active, k)
		end
	end
end

function Quest:update(dt)
	local hasScienceQuest = false
	for k, quest in ipairs(self.active) do
		if quest.type == "scienceQuest" then
			hasScienceQuest = true
		end
		if quest:update(dt) == true then
			self:finish(quest)
		end
	end

	self.nextQuestTime = self.nextQuestTime - dt

	if self.scienceQuest[self.curScienceQuest + 1] ~= nil then
		if self.nextQuestTime <= 0 then
			self.curScienceQuest = self.curScienceQuest + 1
			self:start(self.scienceQuest[self.curScienceQuest])
		end

		if hasScienceQuest == false and self.nextQuestTime <= 0 then
			self.nextQuestTime = math.random(15,100)
			print("Next quest in " .. self.nextQuestTime .. " seconds.")
		end
	end
end

return Quest
