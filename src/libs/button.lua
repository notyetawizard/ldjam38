return function(font, padding, text, x, y)
    local text = love.graphics.newText(font,text)

    local width,height = text:getDimensions()

    local padX = x - padding
    local padY = y - padding
    local padW = width + padding * 2
    local padH = height + padding * 2

    local mouseX, mouseY = love.mouse.getPosition()

    local hover = (mouseX >= padX and mouseX <= padX + padW) and
                  (mouseY >= padY and mouseY <= padY + padH)
    local click = hover and love.mouse.isDown(1)

    if hover then
        love.graphics.setColor(75, 75, 75)
    else
        love.graphics.setColor(50, 50, 50)
    end
    love.graphics.rectangle("fill", padX, padY, padW, padH)

    love.graphics.setColor(255, 255, 255)
    love.graphics.setLineWidth(2)
    love.graphics.rectangle("line", padX, padY, padW, padH)

    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(text, x, y)

    return click
end
