local button = require "libs/button"

Messages = {}
Messages.isDisplaying = false

local curMessage = nil
local fontSize = 16
local font = love.graphics.newFont(fontSize)
local padding = fontSize / 4

function Messages.new(text, callback)
    assert(not Messages.isDisplaying, "Already displaying a message!")

    curMessage = {
        text = text,
        callback = callback
    }

    Messages.isDisplaying = true
end

function Messages.accept()
    Messages.isDisplaying = false
    callback = curMessage.callback
    curMessage = nil
    if callback then
        callback()
    end
end

function Messages.draw()
    if curMessage then
        local text = love.graphics.newText(font, curMessage.text)

        local width,height = text:getDimensions()

        local x = (love.graphics.getWidth() - width)/2
        local y = (love.graphics.getHeight() - height)/2

        local padX = x - padding
        local padY = y - padding
        local padW = width + padding * 2
        local padH = height + padding * 4 + fontSize

        love.graphics.setColor(50, 50, 50)
        love.graphics.rectangle("fill", padX, padY, padW, padH)
        love.graphics.setColor(255, 255, 255)
        love.graphics.setLineWidth(3)
        love.graphics.rectangle("line", padX, padY, padW, padH)

        love.graphics.draw(text, x, y)
        local click = button(font, padding, "OK", x+padding, y+height+padding*2)
        if click then
            Messages.accept()
        end
    end
end

return Messages
