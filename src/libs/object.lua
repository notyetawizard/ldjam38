----------------------------------------------------------------------
-- Object Class & Constructor ----------------------------------------
----------------------------------------------------------------------
local Object = {} -- Class, exported

-- Tile constructor.
function Object.new(options)
	------------------------------------------------------------------
	-- Assertions, to make sure you're using this correctly ----------
	------------------------------------------------------------------
	assert(type(options.name) == "string", "options.name must be a tileset name, as a string.")
	--assert(type(options.image) == "string", "options.image must be a valid filename.")
	--assert(love.filesystem.exists(options.image), "options.image must be a valid filename.")
	assert(type(options.collide) == "boolean", "options.collide must be a booleen.")

	------------------------------------------------------------------
	-- Object Creation -----------------------------------------------
	------------------------------------------------------------------
	local object = {}
	setmetatable(object, Object)
	for k, v in pairs(options) do
		object[k] = v
	end

	--if object.image then object.image = love.graphics.newImage(object.image) end

	return object
end

return Object
