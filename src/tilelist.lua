local Tile = require "libs/tile"

local tilelist = {
	["ground"] = Tile.new{
		name = "ground",
		color = {14, 99, 55},
		collide = false,
		update = function ()

		end
	},

	["wall"] = Tile.new{
		name = "wall",
		color = {237, 47, 14},
		collide = true,
		interact = function (self)
			if not self.inspected then
				Messages.new("Trump must have been here.")
				self.inspected = true
			end
		end
	},

	["water"] = Tile.new{
		name = "water",
		color = {61, 150, 137},
		collide = true,
		interact = function (self)
			if not self.inspected then
				Messages.new("I can't swim")
				self.inspected = true
			end
		end
	}
}

for k, v in pairs(tilelist) do
	assert(k == v.name, "They key for each tile must be the same as it's name.")
end

return tilelist
