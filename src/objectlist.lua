local Object = require "libs/object"
local Image = require "libs/image"
local Message = require "libs/messages"

local objectlist = {
	------------------------------------------------------------------
	-- Generic Stuffs ------------------------------------------------
	------------------------------------------------------------------
	["rock"] = Object.new{
		name = "Rock",
		image = Image.get("rock"),
		collide = true,
		interact = function (self)
			if not self.inspected then
				Messages.new("It's hard as a rock.")
				self.inspected = true
			end

			return {
			}
		end
	},


	------------------------------------------------------------------
	-- Foodstuffs ----------------------------------------------------
	------------------------------------------------------------------
	["apple"] = Object.new{
		name = "Apple",
		image = Image.get("apple"),
		collide = false,
		interact = function ()
			return {
				give = "apple",
				amount = 1,
				remove = true
			}
		end
	},

	------------------------------------------------------------------
	-- Hot Air Balloon -----------------------------------------------
	------------------------------------------------------------------
	["hot_air_balloon"] = Object.new{
		name = "Hot Air Balloon",
		image = nil,
		collide = true,
		interact = function ()
			return {
				give = "Hot Air Balloon",
				amount = 1,
				remove = false
			}
		end
	},
	["hot_air_balloon_basket"] = Object.new{
		name = "Basket",
		image = nil,
		collide = true,
		interact = function ()
			return {
				give = "hot_air_balloon_basket",
				amount = 1,
				remove = true
			}
		end
	},
	["hot_air_balloon_balloon"] = Object.new{
		name = "Balloon",
		image = nil,
		collide = true,
		interact = function ()
			return {
				give = "hot_air_balloon_balloon",
				amount = 1,
				remove = true
			}
		end
	},
	["hot_air_balloon_match"] = Object.new{
		name = "Match",
		image = nil,
		collide = true,
		interact = function ()
			return {
				give = "hot_air_balloon_match",
				amount = 1,
				remove = true
			}
		end
	}
}

return objectlist
