local Player = require "libs/player"
local Tilemap = require "libs/tilemap"
local Quest = require "libs/quest"
local Toolbar = require "libs/toolbar"
local Messages = require "libs/messages"

local tilelist = require "tilelist"
local objectlist = require "objectlist"

local pressedKeys = {}

function love.load()
    math.randomseed(os.time())

    testmap = Tilemap.new("test", 20, 20, tilelist)

    width, height = testmap.getPixelSize()
    height = height + Toolbar.height
    love.window.setMode(width, height)

    player = Player.new(testmap)

    toolbar = Toolbar:new(player)

    testmap.setActor(1, 1, player)
    Quest:setup(testmap, player)
end

function love.mousepressed(x, y, button, istouch)

end

function love.mousereleased(x, y, button, istouch)

end

function love.mousemoved(x, y, dx, dy, istouch)

end

function love.keypressed(key, scan, isrepeat)
    if Messages.isDisplaying then
        if scan == "return" then
            Messages.accept()
        end
    else
        pressedKeys[scan] = true
    end
end

function love.keyreleased(key, scan)
    pressedKeys[scan] = nil
end

function love.update(dt)
    if Messages.isDisplaying then

    else
        for key in pairs(pressedKeys) do
            if key == "left" then
                player:move(-1,0)
                player.dir = math.pi/2*3
            elseif key == "right" then
                player:move(1,0)
                player.dir = math.pi/2
            elseif key == "up" then
                player:move(0,-1)
                player.dir = 0
            elseif key == "down" then
                player:move(0,1)
                player.dir = math.pi
            end
        end

        player:update(dt)
        Quest:update(dt)
    end
end

function love.draw()
    local x = 0
    local y = 0

    love.graphics.push()

    toolbar:draw()
    y = y + Toolbar.height
    love.graphics.translate(x, y)
    testmap.draw()
    local _, mapHeight = testmap:getPixelSize()
    y = y + mapHeight

    love.graphics.pop()
    Messages.draw()
end
